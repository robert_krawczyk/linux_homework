﻿#!/bin/bash

# --- funkcja ktora wyswietla mozliwe dzialania - 
usage(){

	echo  "change_phrase [OPTION] PHRASE1 PHRASE2"
	echo 
	echo " Skrypt zmienia wskazaną frazę PHRASE1 na frazę PHRASE2" 
	echo "w plikach znajdujących się w bieżącym katalogu i jego podkatalogach."
	echo
	echo "Opcje:"
	echo "-h, --help	Wyświetla pomoc"
	echo 
	echo "Przykład: $ change_phrase pupa ***"
}

# --- Zmienne -
argc=$#
req=2

# --- Przetwarzanie opcji -
# --- dla braku parametrów -
if [ $# == 0 ]
then
	usage
	exit 1
fi
# --- dla parametru rozpoczynającego się od "-" -
for i in "$@"
do
case $i in
# --- uruchamia pomoc -
	-h|--help)
	usage
	;;
# --- zwraca błąd braku opcji -
	-*)
	echo "Nieznana opcja, pomoc "-h""
	;;
esac
done
# --- dla właściwych paraetrów -
# --- sprawdza czy występuja dokładnie 2 parametry -
if [ $argc = $req ]; then
	find . -name "*" |find . -type f |xargs perl -pi -e "s/$1/$2/g"
# --- zwraca błąd w przypadku niewłaściwej liczby parametrów -
else
	echo "Podaj 2 parametry, lub użyj pomocy "-h""
fi
