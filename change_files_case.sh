﻿#!/bin/bash

# --- funkcja ktora wyswietla mozliwe dzialania - 
usage(){

	echo
	echo "change_files_case [OPTION] PARAMETER1 PARAMETER2"
	echo 
	echo "Skrypt zmienia wielkość liter nazw plików w bieżącym katalogu."
	echo 
	echo "[PARAMETER1] przyjmuje wartości "to_lower" lub "to_upper""
	echo "PARAMETER2 przyjmuje wartości filtrujące nazwy plików"
	echo
	echo "Opcje:"
	echo "-h, --help	Wyświetla pomoc"
	echo 
	echo "Przykład: $ change_files_case toupper test*"
}

# --- Zmienne -
argc=$#
req=2

# --- Przetwarzanie opcji -
# --- dla braku parametrów -
if [ $# == 0 ]
then
	usage
	exit 1
fi
# --- dla parametru rozpoczynającego się od "-" -
for i in "$@"
do
case $i in
# --- uruchamia pomoc -
	-h|--help)
	usage
	;;
# --- zwraca błąd braku opcji -
	-*)
	echo "Nieznana opcja, pomoc "-h""
	;;
esac
done
# --- dla właściwych parametrów -
# --- sprawdza czy występuja dokładnie 2 parametry -
if [ $argc = $req ]; then

case $1 in
# --- zmienia nazwy z małych na duże litery -
	to_upper)
	for file in $2
	do
		if [ "./$file" != "$0" ] ; then
			mv $file `echo $file | tr [:lower:] [:upper:]`
		fi
	done
	;;
# --- zmienia nazwy z dużych na małe -
	to_lower)
	for file in $2
	do
# --- zabezpieczenie przed zmianą nazwy skryptu -
		if [ "./$file" != "$0" ] ; then
			mv $file `echo $file | tr [:upper:] [:lower:]`
		fi
	done
	;;
# --- zwraca błąd w przypadku innegro parametru nr 1 -
	*)
	echo "Niepoprawny pierwszy parametr, pomoc "-h""
	;;
esac

# --- zwraca błąd w przypadku niewłaściwej liczby parametrów -
else
	echo "Podaj 2 parametry, lub użyj pomocy "-h""
fi
