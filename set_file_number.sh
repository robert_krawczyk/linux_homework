#!/bin/bash

# --- funkcja ktora wyswietla mozliwe dzialania - 
usage(){

	echo
	echo  "set_file_number [OPTION]"
	echo 
	echo "Skrypt dodaje do plików w bieżącym katalogu" 
	echo "przedrostki z kolejnymi numerami."
	echo
	echo
	echo "Opcje:"
	echo "-h, --help	Wyświetla pomoc"
	echo 
	echo "Przykład: $ set_file_number"
}

# --- Przetwarzanie opcji -
# --- dla parametru rozpoczynającego się od "-" -
for i in "$@"
do
case $i in
# --- uruchamia pomoc -
	-h|--help)
	usage
	exit 1
	;;
# --- zwraca błąd braku opcji -
	-*)
	echo "Nieznana opcja, pomoc "-h""
	;;
esac
done
# --- wykonanie zadania -
i=0
for file in *  
	do
# --- omija plik se skryptem -
		if [ "./$file" != "$0" ] ; then
# --- dodaje przedrostek -		
			((i++))	
			mv "$file" $i"_$file" 
		fi
	done


